<?php
/**
 * Created by PhpStorm.
 * User: sascha
 * Date: 20-9-17
 * Time: 11:47
 */

class EduParc
{
    //The base url.
    private $baseUrl = 'http://consis.eduparc.nl/api/v1/';

    //Api token.
    private $token;

    //Access token (same as token).

    function __construct($token,$userToken)
    {
        $this->token = $token;
    }

    /*
     *  Start User actions.
     */

    //Creates a user in the EducParc system.
    public function createUser($email,$firstName,$lastName,$password,$terms,$news = false,$phone = false,$employer = false,$sector = false){

        //Set headers correctly.
        $headers = [
            'authorization: Token token='.$this->token,
            'content-type: application/x-www-form-urlencoded',
            'accept: application/json',
        ];

        //Prepare post data.
        $data = array(
            'user[email]' => $email,
            'user[first_name]' => $firstName,
            'user[last_name]' => $lastName,
            'user[password]' => $password,
            'user[terms]' => $terms,
        );


        if($news != false){
           $data['user[news]'] = $news;
        }

        if($phone != false){
            $data['user[phone]'] = false;
        }
        if($employer != false){
            $data['user[employer]'] = $employer;
        }

        if($sector != false){
            $data['user[sector]'] = $sector;
        }


        return json_decode($this->post("users", $data,$headers));
    }

    //Finds a user in the EduParc system.
    public function findUser($id){

        $headers = [
            'authorization: Token token='.$this->token,
            'accept: application/json',
            'content-type: application/json',
        ];

        return json_decode($this->get('users/'.$id, $headers));
    }

    //Updates a user in the EduParc system.
    public function updateUser($id,$firstName,$lastName,$news,$phone,$employer,$sector){

        $headers = [
            'authorization: Token token='.$this->token,
            'content-type: application/json',
        ];

        $data = array(
            'user[first_name]' => $firstName,
            'user[last_name]' => $lastName,
            'user[news]' => $news,
            'user[phone]' => $phone,
            'user[employer]' => $employer,
            'user[sector]' => $sector,
        );

        return json_decode($this->put('users/'.$id,$data,$headers));
    }

    /*
     *  End User actions.
     */


    /*
     *  Start Material actions.
     */


    public function findMaterial($id){

        $headers = [
            'authorization: Token token='.$this->token,
            'accept: application/json',
            'content-type: application/x-www-form-urlencoded',
        ];

        return json_decode($this->get('course_materials/'.$id, $headers));
    }

    public function getMaterials(){

        $headers = [
            'authorization: Token token='.$this->token,
            'accept: application/json',
            'content-type: application/x-www-form-urlencoded',
        ];

        return json_decode($this->get('course_materials/', $headers));
    }

    /*
     *  End Material actions.
     */



    /*
     *  Start course actions.
     */


    public function getCourses(){
        $headers = [
            'authorization: Token token='.$this->token,
            'accept: application/json',
            'content-type: application/x-www-form-urlencoded',
        ];

        return json_decode($this->get('search_courses/', $headers));
    }

    public function getCoursesFiltered($available){

        $headers = [
            'authorization: Token token='.$this->token,
            'accept: application/json',
            'content-type: application/x-www-form-urlencoded',
        ];
        $availableString = "true";
        if($available == true){
            $availableString = true;
        }
        else{
            $availableString = false;
        }

        return json_decode($this->get('search_courses?sessions[available]='.$availableString, $headers));
    }




    public function bookingMaterials($materials, $credit,$userToken){

        $headers = [
            'authorization: Token token='.$this->token,
            'accept: application/json',
            'content-type: application/x-www-form-urlencoded',
            'access-token: '.$userToken,
        ];

        $body = array(
            'lesson_definition_material_ids[]' => $materials,
            'credit' => $credit,
        );

        return json_decode($this->post('booked_materials/',$body,$headers));

    }

    public function bookingSession($lessonDate,$credit,$hourNotification,$dayNotification,$userToken){
        $headers = [
            'authorization: Token token='.$this->token,
            'accept: application/json',
            'content-type: application/x-www-form-urlencoded',
            'access-token: '.$userToken,
        ];

        $body = array(
            'credit' => $credit,
            'lesson_student[hour_notification]' => $hourNotification,
            'lesson_student[day_notification]' => $dayNotification,
        );

        return json_decode($this->post('booked_sessions/',$body,$headers));
    }

    /*
     *  End Course actions
     */


    /*
     *  Start HTTP actions.
     */

    //Handles all POST requests.
    private function post($endPoint,$data,$headers){
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $this->baseUrl.$endPoint,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => http_build_query($data),
            CURLOPT_HTTPHEADER => $headers,
        ));
        // Send the request & save response to $resp
        $response = curl_exec($curl);
        // Close request to clear up some resources
        curl_close($curl);
        return $response;
    }

    //Handles all GET requests.
    private function get($endPoint,$headers){
        // Get cURL resource
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $this->baseUrl.$endPoint,
            CURLOPT_HTTPHEADER => $headers,
        ));
        // Send the request & save response to $resp
        $response = curl_exec($curl);
        // Close request to clear up some resources
        curl_close($curl);
        return $response;
    }

    //Handles all PUT requests.
    private function put($endPoint, $data,$headers){
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_CUSTOMREQUEST => "PUT",
            CURLOPT_URL => $this->baseUrl.$endPoint,
            CURLOPT_POSTFIELDS => http_build_query($data),
            CURLOPT_HTTPHEADER => $headers,
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }

    /*
     *  End HTTP actions.
     */
}
